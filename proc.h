/* File:	proc.h
 * Created: 	2012 
 * License:	GPL version 3
 * Author:
 *		Francieli Zanon Boito <francielizanon (at) gmail.com>
 *
 * Description:
 *		This file is part of the AGIOS I/O Scheduling tool.
 *		It handles scheduling statistics and print them whenever the agios_print_stats_file 
 *		function is called. Stats can be reseted by agios_reset_stats.
 *		The kernel module implementation uses the proc interface, while the user-level
 *		library creates a file in the local file system. Its name is provided as a parameter.
 *		Further information is available at http://inf.ufrgs.br/~fzboito/agios.html
 *
 * Contributors:
 *		Federal University of Rio Grande do Sul (UFRGS)
 *		INRIA France
 *
 *		inspired in Adrien Lebre's aIOLi framework implementation
 *	
 *		This program is distributed in the hope that it will be useful,
 * 		but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */


#ifndef PROC_H
#define PROC_H

#include <stdlib.h>
#include <stdio.h>
#include "agios_request.h"

void stats_aggregation(struct related_list_t *related);
void stats_shift_phenomenon(struct related_list_t *related);
void stats_better_aggregation(struct related_list_t *related);
void reset_global_reqstats(void);
void stats_predicted_better_aggregation(struct related_list_t *related);
void proc_stats_newreq(struct request_t *req);

void reset_stats_window(void);

void proc_stats_init(void);
void proc_stats_exit(void);

void proc_set_needs_hashtable(short int value);
void proc_set_new_algorithm(int alg);
unsigned long int *proc_get_alg_timestamps(int *start, int *end);
void update_avg_stats_field(unsigned long int value, unsigned long int counter, float *avg);

void initialize_twins_metrics(void);
void clean_twins_metrics(void);
void prepare_twins_metrics(char *fd); //struct council_metrics_t *metrics);
void reset_twins_metrics(void);
#endif

