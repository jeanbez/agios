AGIOS' code can be retrieved from \url{https://bitbucket.org/francielizanon/agios/wiki/AGIOS-1.1.tgz}. You will need to download it, compile and install.

\begin{lstlisting}[language=bash]
$ tar xzf AGIOS-1.1.tgz
$ cd AGIOS-1.1/
$ make clean
$ make library
$ make library_install
\end{lstlisting}

The commands above will install the library in \emph{/usr/lib/} and copy its header - \emph{agios.h} -  to \emph{/usr/include}. It requires that \emph{Libconfig} is installed, you can get it from \url{http://www.hyperrealm.com/libconfig/}. 

AGIOS can then be used in your I/O service's code through:

\begin{lstlisting}[language=C]
#include <agios.h>
\end{lstlisting}

\section{Initialization}

As previously discussed, AGIOS' interface is composed of two main steps: requests are added to the scheduler, and a callback is used to notify the user it is time to process requests. 

The first thing is to initialize the library. A \emph{struct client} must be filled before calling \emph{agios\_init}:

\begin{lstlisting}[language=C]
struct client {
	void (*process_request)(int i);
	void (*process_requests)(int *i, int reqnb);
	short int sync; 
};
\end{lstlisting}

The user must provide at least the \emph{process\_request} callback, used by the scheduler to process a request. As previously discussed, a callback to process multiple requests at once can also be provided: \emph{process\_requests}. 

Notice that requests are identified between user and scheduler through an integer value. This value is managed by the user and provided to the scheduler in the \emph{agios\_add\_request} call.

\begin{lstlisting}[language=C]
int main()
{
	struct client clnt;
	char *config_filename = "/tmp/agios.conf";

	clnt.process_request = my_process_request_function;
	clnt.process_requests = NULL;

	agios_init(&clnt, config_filename);

	...
}
\end{lstlisting}

The above code initializes the library, passing a callback function to process a request, but not to process multiple requests at once (in this case it should be set to NULL). 

\begin{lstlisting}[language=C]
int agios_init(struct client *clnt, char *config_file);
\end{lstlisting}

Notice that the \emph{agios\_init} call also requires the name for a configuration file. Chapter~\ref{chapter:configfile} discusses the configuration file.

The initialization function will return $0$ in case of success and something else in case of errors.

\begin{lstlisting}[language=C]
	agios_init(&clnt, config_filename);
	if(clnt.sync)
		...
\end{lstlisting}

During initialization, AGIOS will decide which I/O scheduling algorithm to use. Some of them require synchronous scheduling, i.e. one request must be processed at a time, and the scheduler will only choose more requests after the previous were processed. The responsibility of making this synchronization is left to users in order to keep the library as generic as possible. An example of how this can be done will be provided in the next section.

\section{Scheduling requests}

When requests arrive to the user, they must be provided to AGIOS through the \emph{agios\_add\_request} call.

\begin{lstlisting}[language=C]
int agios_add_request(char *file_id, int type, 
		      long long offset,long len, 
		      int data, struct client *clnt);
\end{lstlisting}

The first parameter - \emph{file\_id} - is an identifier of the file this request is for. It is a good idea not to use simply a file handler but an unique identifier that will always be the same to each file (such as the file's name). 

The request's \emph{type} must be \emph{RT\_READ} or \emph{RT\_WRITE}, according to the operation being performed.

\emph{Offset} and \emph{len} give the file position where to start the operation and its size, respectively.

\emph{Data} is the integer value that identifies the request to the user. It will be provided by the scheduler when processing this request. For instance, if requests are added to an array in the user, a good choice would be to provide the request's index in this parameter.

The \emph{struct client} parameter is the same initialized and provided to \emph{agios\_init}.

Once requests are ready to be processed, the scheduler will call the \emph{process\_request} callback previously discussed, giving back the provided \emph{data} parameter of the requests. This function should return as soon as possible so the scheduler can continue to select requests to be processed, unless the field \emph{sync} from the struct client was set to $1$ during the initialization phase. In this case, the library's user must ensure the synchronization. The code below provides a simplified example of how this can be done with \emph{pthread} condition variables.

\begin{lstlisting}[language=C, breaklines=true]
#include <pthread.h>
#include <agios.h>

#define CONFIG_FILE "/tmp/agios.conf"

short int agios_can_continue=0;
pthread_cond_t req_processed_cond = 
				PTHREAD_COND_INITIALIZER;
pthread_mutex_t req_processed_mutex = 
			       PTHREAD_MUTEX_INITIALIZER;
struct client clnt;

void receiver_thread()
{
   int index, type;
   char *file_id;
   long long offset;
   long len;

   do {
      index = receive_request_from_client
			(&file_id, &type, &offset, &len);
      agios_add_request
	       (file_id, type, offset, len, data, &clnt);
   } while(!end_of_execution());
}

void io_thread()
{
   int index;

   do {
      index = wait_for_ready_requests();
      process_ready_request(index);

      /*notify that AGIOS can proceed*/
      if(clnt.sync)
      {
         pthread_mutex_lock(&req_processed_mutex);
	 agios_can_continue=1;
	 pthread_cond_signal(&req_processed_cond);
	 pthread_mutex_unlock(&req_processed_mutex);			
      }
   } while(!end_of_execution());
}

void my_process_request_callback(int i)
{
   notify_io_thread_about_ready_request(i);

   /* wait until it was processed before AGIOS 
    * can schedule more requests */
   if(clnt.sync)
   {
      pthread_mutex_lock(&req_processed_mutex);
      while(!agios_can_continue)
         pthread_cond_wait
	     (&req_processed_cond, &req_processed_mutex);
      agios_can_continue=0;
      pthread_mutex_unlock(&req_processed_mutex);
   }
}

int main()
{
   /* initialize AGIOS*/
   clnt.process_request = my_process_request_callback;
   clnt.process_requests = NULL;
   it(agios_init(&clnt, CONFIG_FILE) != 0)
   {
	exit_on_error();
   }

   /*initialize threads*/
   start_receiver_thread();
   start_io_thread();

   ...
}

\end{lstlisting}

In this example, the I/O service has two main threads: the receiver thread, responsible for receiving requests from the I/O service's clients, and the I/O thread, which treats the requests (performs I/O). The receiver thread gives incoming requests to AGIOS, which calls \emph{my\_process\_request\_callback()} when they are ready to be processed. This callback will notify the I/O thread, that will actually process requests. A \emph{pthread} condition variable (associated with a mutex) ensures the synchronous behavior when required by AGIOS.

\section{Generating scheduling statistics}

During execution, the AGIOS' user can ask the library to generate statistics files through the \emph{agios\_print\_stats\_file} call. This call may take a long time if called too soon after the library's initialization, before the Prediction Module is initialized (because it will have to wait for it).

\begin{lstlisting}[language=C]
void agios_print_stats_file(char *filename);
\end{lstlisting}

Statistics can be reset by calling \emph{agios\_reset\_stats}. If trace files are being generated, this call will result in closing the actual trace file and starting a new one. If the Prediction Module is reading from trace files, it will take the just-closed trace file and consider it in its decisions. More detail on trace files and the Prediction Module will be presented in Chapter~\ref{chapter:trace}.

\begin{lstlisting}[language=C]
void agios_reset_stats(void);
\end{lstlisting}

\section{Ending the execution}

When finishing the I/O service execution, the \emph{agios\_exit} function must be called.

\begin{lstlisting}[language=C]
void agios_exit(void);
\end{lstlisting}
