/* File:	TWINS.c
 * Created: 	2016
 * License:	GPL version 3
 * Author:
 *		Francieli Zanon Boito <francielizanon (at) gmail.com>
 * Collaborators:
 *		Jean Luca Bez <jlbez (at) inf.ufrgs.br>
 *
 * Description:
 *		This file is part of the AGIOS I/O Scheduling tool.
 *		It provides the exclusive time window scheduling algorithm
 *		Further information is available at http://agios.bitbucket.org/
 *
 * Contributors:
 *		Federal University of Rio Grande do Sul (UFRGS)
 *		Federal University of Santa Catarina (UFSC)
 *		INRIA France
 *
 *		inspired in Adrien Lebre's aIOLi framework implementation
 *	
 *		This program is distributed in the hope that it will be useful,
 * 		but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef AGIOS_KERNEL_MODULE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#else
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/time.h>
#include <linux/slab.h>
#endif

//socket
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#include "agios.h"
#include "TWINS.h"
#include "request_cache.h"
#include "consumer.h"
#include "iosched.h"
#include "common_functions.h"
#include "req_timeline.h"
#include "hash.h"
#include "agios_request.h"
#include "agios_config.h"
#include "proc.h"

static int twins_first_req;
static int current_window;
static struct timespec window_start;
static unsigned long int processing; //to keep track of time spent processing requests in each window 

//dynamic twins thread
static pthread_t twins_thread;
int twins_thread_stop = 0;
pthread_mutex_t twins_thread_stop_mutex = PTHREAD_MUTEX_INITIALIZER;

//socket
int sockfd, port, n;
struct sockaddr_in server_address;
struct hostent *server;

char buffer[128];
char window_buffer[8];

int new_window = 250000L;

void open_socket_to_twins_council(void)
{
	debug("opening socket to TWINS council...");

	port = config_dynamic_twins_council_port;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0) {
		debug("ERROR opening socket to TWINS council");
	} else {
		debug("SUCCESS opening to TWINS council...");
	}

	server = gethostbyname(config_dynamic_twins_council_host);

	if (server == NULL) {
		agios_print("ERROR: unable to connect to the council (no such host)\n");
		exit(0);
	}

	bzero((char *) &server_address, sizeof(server_address));
	server_address.sin_family = AF_INET;
	bcopy((char *)server->h_addr,
		 (char *)&server_address.sin_addr.s_addr,
		 server->h_length);
	server_address.sin_port = htons(port);

	if (connect(sockfd,(struct sockaddr *) &server_address,sizeof(server_address)) < 0) {
		agios_print("ERROR connecting to TWINS council");
	} else {
		debug("SUCCESS connecting with TWINS council...");
	}
}

void close_socket_to_twins_council(void)
{
	close(sockfd);
}

int meeting_with_twins_council()
{
	debug("inviting meeting with TWINS council...");

	n = write(sockfd, buffer, strlen(buffer));
	if (n < 0) {
		agios_print("ERROR writing to socket");
	} /*else {
		agios_print("SUCCESS writing to socket");
	}*/

	n = recv(sockfd, window_buffer, 8, MSG_WAITALL);

	if (n < 0) {
		agios_print("ERROR reading from socket");
	} /*else {
		agios_print("SUCCESS reading from socket");
	}*/

	return atoi(window_buffer);
}

void stop_the_twins_thread(void)
{
	agios_mutex_lock(&twins_thread_stop_mutex);
	twins_thread_stop = 1;
	agios_mutex_unlock(&twins_thread_stop_mutex);
}

int twins_thread_should_stop(void)
{
	int ret;

	agios_mutex_lock(&twins_thread_stop_mutex);
	ret = twins_thread_stop;
	agios_mutex_unlock(&twins_thread_stop_mutex);

	return ret;
}

//for now, this thread periodically write statistics to a file
void *TWINS_thread(void *arg)
{
	struct timespec start, t1;
	unsigned long int elapsed, period;

	int has_just_changed = 0;
	
	agios_gettime(&start);
	period = config_dynamic_twins_period*1000000; //twins_period is in ms, well use it in ns;
	while(!twins_thread_should_stop())
	{
		
		elapsed = get_nanoelapsed(start); 
		if(elapsed < period)
		{
			//sleep for the remaining time
			agios_wait((unsigned int) (period - elapsed), NULL);
			elapsed = get_nanoelapsed(start); 
		}

		if(elapsed > period)  //NOT an else because we could enter the first if, sleep, update elapsed, and now we'll enter this if
		{
			agios_gettime(&t1);
			debug("time to prepare TWINS metrics...");
			
			prepare_twins_metrics(buffer);

			// we will only send new metrics after a full observation window, so that we have time to proccess
			// the rest of the requests in queue and give time for the new window to stabelize the execution
		
			if (!has_just_changed) {
				// send the message to the server using a socket
				new_window = meeting_with_twins_council();

				debug("TWINS: %d\n", new_window);

				debug("twins thread took %lu ns to meet with the council", get_nanoelapsed(t1));

				// check if we need to change the window after updating the usage of the previous current window
				if (config_twins_window_duration != new_window * 1000L) {
					debug("update TWINS window from %lu to %lu", config_twins_window_duration, new_window * 1000L);

					// we need to get the lock on the timelimit
					timeline_lock();

					// update the window, council returns in ms thus we need to conver to ns
					config_twins_window_duration = new_window * 1000L;

					// release the lock on the timeline
					timeline_unlock();

					has_just_changed = 1;
				}
			} else {
				has_just_changed = 0;
			}

			reset_twins_metrics();
			agios_gettime(&start); // we start running the clock for the next period only after we did what we needed to do
		}
	}

	return NULL;
}

//returns 1 on success
int TWINS_init()
{
	int ret=1;

	twins_first_req = 1; //we'll start the first time window only when the first request is selected for processing (in the future we might want to reset it every once in a while)
	current_window = 0; //the first id we will prioritize
	//sanity check
	if(app_timeline_size <= 0)
	{
		agios_print("To use TWINS, you need to provide a maximum app id that is higher than 0 so I can allocate memory...");
		return 0;
	}
	if(config_twins_window_duration <= 0)
	{
		agios_print("non-sensical value for twins_window_duration");
		return 0;
	}
	
	//if we want twins to dynamically adapt, we need to create his thread
	if(config_dynamic_twins)
	{
		ret = pthread_create(&twins_thread, NULL, TWINS_thread, NULL);
		if(ret != 0)
		{
			agios_print("Sorry, could not create thread for dynamic TWINS!");
			config_dynamic_twins = 0;
		}
		ret = 1;

		//we also need to create an open the socket with the server, if dynamic twins is enabled
		open_socket_to_twins_council();
	}

	return ret;
}
void TWINS_exit()
{
	//stop the thread
	if(config_dynamic_twins)
	{
		stop_the_twins_thread();
		pthread_join(twins_thread, NULL);	

		//close the socket with the server, if dynamic twins is enabled
		close_socket_to_twins_council();
	}
}

void TWINS(void *clnt)
{
	short int update_time=0;
	struct request_t *req;
	unsigned long hash;
	struct timespec t1;
	
	
	PRINT_FUNCTION_NAME;
	//we are not locking the current_reqnb_mutex, so we could be using outdated information. We have chosen to do this for performance reasons (and because being wrong sometimes is not a big deal)
	while((current_reqnb > 0) && (update_time == 0))
	{
		timeline_lock();

		//do we need to setup the window, or did it end already?
		if(twins_first_req) 
		{
			//we are going to start the first window!
			agios_gettime(&window_start);
			twins_first_req = 0;
			current_window = 0;
			processing=0;
			debug("TWINS is starting with windows of %lu\n", config_twins_window_duration);
		}
		else if(get_nanoelapsed(window_start) >= config_twins_window_duration)
		{
			//we're done with this window, time to move to the next one
			agios_gettime(&window_start);
			processing = 0;
			current_window++;
			if(current_window >= app_timeline_size)
				current_window = 0; //round robin!
			debug("time is up, moving on to window %d", current_window);
		}
	
		//process requests!
		if(!(agios_list_empty(&(app_timeline[current_window])))) //we can only process requests from the current app_id
		{	
			agios_gettime(&t1);
			//take request from the right queue
			req = agios_list_entry(app_timeline[current_window].next, struct request_t, related);
			//remove from the queue
			agios_list_del(&req->related);

			/*sends it back to the file system*/
			//we need the hash for this request's file id so we can update its stats 
			hash = AGIOS_HASH_STR(req->file_id) % AGIOS_HASH_ENTRIES;
			update_time = process_requests(req, (struct client *)clnt, hash);
			generic_post_process(req);
			processing +=get_nanoelapsed(t1); //we keep track of the time we spend processing requests 
		}
		else
		{
			//if there are no requests for this queue, we could sleep a little so we won't be in busy waiting
			//TODO (if we really need to) <- if we do, we need to free the timeline lock before sleeping otherwise the client won't be able to add new requests

		}

		timeline_unlock();

	}
}
