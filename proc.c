/* File:	proc.c
 * Created: 	2012 
 * License:	GPL version 3
 * Author:
 *		Francieli Zanon Boito <francielizanon (at) gmail.com>
 *
 * Description:
 *		This file is part of the AGIOS I/O Scheduling tool.
 *		It handles scheduling statistics and print them whenever the agios_print_stats_file 
 *		function is called. Stats can be reseted by agios_reset_stats.
 *		The kernel module implementation uses the proc interface, while the user-level
 *		library creates a file in the local file system. Its name is provided as a parameter.
 *		Further information is available at http://inf.ufrgs.br/~fzboito/agios.html
 *
 * Contributors:
 *		Federal University of Rio Grande do Sul (UFRGS)
 *		INRIA France
 *
 *		inspired in Adrien Lebre's aIOLi framework implementation
 *	
 *		This program is distributed in the hope that it will be useful,
 * 		but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
#include "agios.h"
#include "agios_request.h"
#include "proc.h"
#include "mylist.h"
#include "common_functions.h"
#include "trace.h"
#include "request_cache.h"
#include "predict.h"
#include "agios_config.h"
#include "req_hashtable.h"
#include "req_timeline.h"
#include "performance.h"
#include "hash.h"

#define NUMBER_OF_IOFSL_SERVERS 4
#define TWINS_NN_MODEL_WRITE 0
#define TWINS_NN_MODEL_READ 1

#ifdef AGIOS_KERNEL_MODULE
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#else
#include <string.h>
#include <limits.h>
#include <assert.h>
#endif
/***********************************************************************************************************
 * GLOBAL ACCESS PATTERN STATISTICS *
 ***********************************************************************************************************/
static struct timespec *last_req=NULL; 
static struct timespec *last_read_req=NULL; 
static struct timespec *last_write_req=NULL; 
static struct agios_global_statistics_t stats_for_file;
static struct agios_global_statistics_t stats_for_window;
static pthread_mutex_t global_statistics_mutex = PTHREAD_MUTEX_INITIALIZER;



/***********************************************************************************************************
 * LOCAL COPIES OF PARAMETERS *
 ***********************************************************************************************************/
static int *proc_algs; //the list of the last PROC_ALGS_SIZE selected scheduling algorithms
static unsigned long int *proc_algs_timestamps; //the timestamps of the last PROC_ALGS_SIZE algorithms selections
static int proc_algs_start, proc_algs_end=0; //indexes to access the proc_algs list 

void proc_set_new_algorithm(int alg)
{
	struct timespec now;

	agios_gettime(&now);
	proc_algs[proc_algs_end] = alg;
	proc_algs_timestamps[proc_algs_end] = get_timespec2llu(now);
	proc_algs_end++;
	if(proc_algs_end >= config_agios_proc_algs)
		proc_algs_end=0; //circular list
	if(proc_algs_start == proc_algs_end)
	{
		proc_algs_start++; 	//we remove the oldest
		if(proc_algs_start >= config_agios_proc_algs)
			proc_algs_start = 0;
	}
}
unsigned long int *proc_get_alg_timestamps(int *start, int *end)
{
	*start = proc_algs_start;
	*end = proc_algs_end;
	return proc_algs_timestamps;
}



/***********************************************************************************************************
 * STATISTICS FILE *
 ***********************************************************************************************************/
/*the user-level library uses a file, while the kernel module implementation uses the proc file system interface*/
#ifndef AGIOS_KERNEL_MODULE
static FILE *stats_file;  
#else
static struct seq_file *stats_file; //to help while writing the file
static struct proc_dir_entry *agios_proc_dir;
static struct file_operations stats_proc_ops;
static struct file_operations predicted_stats_proc_ops;
#ifdef AGIOS_DEBUG
static struct file_operations hashtable_proc_ops;
#endif
#endif
static int hash_position;


/***********************************************************************************************************
 * FUNCTIONS TO UPDATE STATISTICS UPON EVENTS *
 ***********************************************************************************************************/

void update_avg_stats_field(unsigned long int value, unsigned long int counter, float *avg)
{
	if(counter == 1)
	{
		*avg = value;
	}
	else
	{
		//iterative mean: m(t) = ((t-1)/t)*m(t-1) + (1/t)*xt
		float t = (float)counter;
		*avg = (((t-1.0)/t)*(*avg)) + ((1.0/t)*((float)value));
	}
} 
void update_stats_field(unsigned long int value, unsigned long int counter, float *avg, unsigned long int *maxi, unsigned long int *mini)
{
	update_avg_stats_field(value, counter, avg);
	if(value > *maxi)
		*maxi = value;
	if(value < *mini)
		*mini = value;
	
}
void update_local_queue_stats(unsigned long int elapsedtime, long int this_distance, struct queue_statistics_t *stats, struct request_t *req)
{
	if(stats->receivedreq_nb > 1) // we don't update the metrics that are calculated between consecutive requests when we receive the first request
	{
		//time between requests
		update_stats_field(elapsedtime, stats->receivedreq_nb-1, &(stats->avg_request_time), &(stats->max_request_time), &(stats->min_request_time));  //we use receivedreq_nb -1 because we dont update these metrics when we receive the first request

		//offset distance
		update_avg_stats_field((unsigned long int)this_distance, stats->receivedreq_nb-1, &(stats->avg_distance));
	}	

	//request size
	stats->total_req_size+=req->io_data.len;
	if(req->io_data.len > stats->max_req_size)
		stats->max_req_size = req->io_data.len;
	if(req->io_data.len < stats->min_req_size)
		stats->min_req_size = req->io_data.len;
}


void update_global_stats_newreq(struct agios_global_statistics_t *stats, struct request_t *req, unsigned long int elapsedtime, unsigned long int elapsedreadtime, unsigned long int elapsedwritetime)
{

	stats->total_reqnb++;
	if(req->type == RT_READ)
		stats->total_readnb++;
	else
		stats->total_writenb++;
	//update global statistics on time between requests
	if(stats->total_reqnb > 1) //well not update averages on the first measurement (because it needs to be between consecutive requests) 
	{
		update_stats_field(elapsedtime, stats->total_reqnb-1, &(stats->avg_req_time), &(stats->max_req_time), &(stats->min_req_time)); //we use reqnb - 1 because we will not update averages on the first request, the first average is computed when the second request arrives (and that is average number 1)
		if((req->type == RT_READ) && (stats->total_readnb > 1))
			update_stats_field(elapsedreadtime, stats->total_readnb-1, &(stats->avg_read_req_time), &(stats->max_read_req_time), &(stats->min_read_req_time));
		else if((req->type == RT_WRITE) && (stats->total_writenb > 1))
			update_stats_field(elapsedwritetime, stats->total_writenb-1, &(stats->avg_write_req_time), &(stats->max_write_req_time), &(stats->min_write_req_time));
	}
	//update global statistics on request size
	update_stats_field(req->io_data.len, stats->total_reqnb, &(stats->avg_req_size), &(stats->max_req_size), &(stats->min_req_size)); //here we don't use reqnb - 1 because we compute the first average when we receive the first request
	if(req->type == RT_READ)
		update_stats_field(req->io_data.len, stats->total_readnb, &(stats->avg_read_req_size), &(stats->max_read_req_size), &(stats->min_read_req_size));
	else
		update_stats_field(req->io_data.len, stats->total_writenb, &(stats->avg_write_req_size), &(stats->max_write_req_size), &(stats->min_write_req_size));
} 
unsigned long int get_elapsedtime_and_update(struct timespec **last, unsigned long int *this)
{
	unsigned long int ret = 0;
	if(*last == NULL)
	{
		*last = malloc(sizeof(struct timespec));
		if(*last == NULL)
		{
			agios_print("PANIC! Cannot allocate memory for keeping statistics!");
			return 0;
		}	
	}
	else
		ret = ((*this) - get_timespec2llu(**last)) / 1000; //nano to micro
	get_llu2timespec(*this, *last);
	return ret;
}
long int get_offsetdistance_and_update(struct request_t *req, unsigned long int *last)
{
	long int ret;
	ret = req->io_data.offset - (*last);
	if(ret < 0)
		ret = 0 - ret;
	*last = req->io_data.offset + req->io_data.len;
	return ret;	
}
//update the statistics that are kept for each file (not for each queue)
void update_local_file_stats(unsigned long int elapsedtime, long int this_distance, struct file_statistics_t *stats)
{
	stats->counter++;

	if(stats->counter > 1) //we don't update averages for the first request (because they are calculated between consecutive requests)
	{
		//update time between requests
		update_stats_field(elapsedtime, stats->counter -1, &(stats->avg_request_time), &(stats->max_request_time), &(stats->min_request_time)); //we use counter - 1 because we dont update when we receive the first request

		//update average offset distance
		update_avg_stats_field((unsigned long int)this_distance, stats->counter-1, &(stats->avg_distance));
	}
}
/*update the stats after the arrival of a new request
 * must hold the hashtable mutex 
 */
void proc_stats_newreq(struct request_t *req)
{
	unsigned long int elapsedtime=0;
	unsigned long int elapsedreadtime=0;
	unsigned long int elapsedwritetime=0;
	long int this_distance;

	req->globalinfo->stats_file.receivedreq_nb++; 
	req->globalinfo->stats_window.receivedreq_nb++; 

	if(req->state == RS_PREDICTED)
	{
		req->globalinfo->stats_file.processedreq_nb++; 
		req->globalinfo->stats_window.processedreq_nb++; 
	}
	else //if req->state is RS_HASHTABLE
	{
		//update global statistics
		agios_mutex_lock(&global_statistics_mutex);

		elapsedtime = get_elapsedtime_and_update(&last_req, &(req->jiffies_64));
		if(req->type == RT_READ)
			elapsedreadtime = get_elapsedtime_and_update(&last_read_req, &req->jiffies_64);
		else
			elapsedwritetime = get_elapsedtime_and_update(&last_write_req, &req->jiffies_64);

		update_global_stats_newreq(&stats_for_file, req, elapsedtime, elapsedreadtime, elapsedwritetime); 
		update_global_stats_newreq(&stats_for_window, req, elapsedtime, elapsedreadtime, elapsedwritetime);  

		agios_mutex_unlock(&global_statistics_mutex);

		//update statistics for the file
		elapsedtime = get_elapsedtime_and_update(&(req->globalinfo->req_file->last_req), &(req->jiffies_64));
		this_distance = get_offsetdistance_and_update(req, &(req->globalinfo->req_file->last_final_offset));

		update_local_file_stats(elapsedtime, this_distance, &(req->globalinfo->req_file->stats_file));
		update_local_file_stats(elapsedtime, this_distance, &(req->globalinfo->req_file->stats_window));

		//update statistics for the queue
		elapsedtime = get_elapsedtime_and_update(&(req->globalinfo->last_req_time), &(req->jiffies_64));
		this_distance = get_offsetdistance_and_update(req, &(req->globalinfo->last_received_finaloffset));
 
		update_local_queue_stats(elapsedtime, this_distance, &(req->globalinfo->stats_file), req);
		update_local_queue_stats(elapsedtime, this_distance, &(req->globalinfo->stats_window), req);
	}
}

/*reset some global stats*/
void _reset_global_reqstats(struct agios_global_statistics_t *stats)
{
	stats->total_reqnb=0;
	stats->total_readnb=0;
	stats->total_writenb=0;
	stats->avg_req_time = 0.0;
	stats->avg_read_req_time = 0.0;
	stats->avg_write_req_time = 0.0;
	stats->min_req_time = ~0;
	stats->min_read_req_time = ~0;
	stats->min_write_req_time = ~0;
	stats->max_req_time = 0;
	stats->max_read_req_time = 0;
	stats->max_write_req_time = 0;
	stats->avg_req_size = 0;
	stats->avg_read_req_size = 0;
	stats->avg_write_req_size = 0;
	stats->min_req_size = ~0;
	stats->min_read_req_size = ~0;
	stats->min_write_req_size = ~0;
	stats->max_req_size=0;
	stats->max_read_req_size=0;
	stats->max_write_req_size=0;
}
void reset_global_reqstats(void) //called by request_cache_init() in the beginning of the execution
{
	agios_mutex_lock(&global_statistics_mutex);
	_reset_global_reqstats(&stats_for_file);
	_reset_global_reqstats(&stats_for_window);
	agios_mutex_unlock(&global_statistics_mutex);
}
void reset_global_reqstats_window(void)
{
	agios_mutex_lock(&global_statistics_mutex);
	_reset_global_reqstats(&stats_for_window);
	agios_mutex_unlock(&global_statistics_mutex);
}
void reset_global_reqstats_file(void)
{
	agios_mutex_lock(&global_statistics_mutex);
	_reset_global_reqstats(&stats_for_file);
	agios_mutex_unlock(&global_statistics_mutex);
}
/*reset statistics used for scheduling algorithm selection*/
//this is called while holding the migration mutex algorithm, so no other mutexes are necessary
//must at least hold the hashtable line mutex
void reset_stats_window_related_list(struct related_list_t *related_list)
{
	related_list->stats_window.processedreq_nb = 0;
	related_list->stats_window.receivedreq_nb = 0;
	related_list->stats_window.processed_req_size = 0;
	related_list->stats_window.processed_req_time = 0;

	related_list->stats_window.total_processed_req_bandwidth = 0;
	//related_list->stats_window.min_processed_req_bandwidth = ~0;
	//related_list->stats_window.max_processed_req_bandwidth = 0;

	related_list->stats_window.total_req_size=0;
	related_list->stats_window.min_req_size=~0;
	related_list->stats_window.max_req_size=0;

	related_list->stats_window.max_request_time = 0;
	related_list->stats_window.avg_request_time = 0.0;
	related_list->stats_window.min_request_time = ~0;

	related_list->stats_window.avg_distance=0.0;

	related_list->stats_window.aggs_no = 0;
	related_list->stats_window.sum_of_agg_reqs = 0;
}
void reset_stats_window_file(struct request_file_t *req_file)
{
	req_file->stats_window.avg_request_time = 0.0;
	req_file->stats_window.max_request_time = 0;
	req_file->stats_window.min_request_time = ~0;
	req_file->stats_window.avg_distance = 0.0;
	req_file->stats_window.counter = 0;
}
//this function is called in consumer.c when changing the scheduling algorithm, after locking all data structures in the change_selected_alg function. Therefore we don't need to worry about accessing the hashtable as it is safe.
void reset_stats_window(void)
{
	int i;
	struct agios_list_head *list;
	struct request_file_t *req_file;

	for(i=0; i< AGIOS_HASH_ENTRIES; i++)
	{
		list = &hashlist[i];
		agios_list_for_each_entry(req_file, list, hashlist)
		{
			reset_stats_window_related_list(&req_file->related_reads);
			reset_stats_window_related_list(&req_file->related_writes);
			reset_stats_window_file(req_file);
		}
	}
	reset_global_reqstats_window();
}



/*updates the stats after an aggregation*/
void stats_aggregation(struct related_list_t *related)
{
	if (related->lastaggregation > 1) {
		related->stats_file.aggs_no++;
		related->stats_file.sum_of_agg_reqs += related->lastaggregation;
		related->stats_window.aggs_no++;
		related->stats_window.sum_of_agg_reqs += related->lastaggregation;
		if (related->best_agg < related->lastaggregation)
			related->best_agg = related->lastaggregation;
	}
}
/*updates the stats after the detection of a shift phenomenon*/
void stats_shift_phenomenon(struct related_list_t *related)
{
	related->shift_phenomena++;
}
/*updates the stats after the detection that a better aggregation is possible (by looking to the current stats for the file)*/
void stats_better_aggregation(struct related_list_t *related)
{
	related->better_aggregation++;
}
/*updates the stats after the detection that a better aggregation is possible (by looking at the predictions)*/
void stats_predicted_better_aggregation(struct related_list_t *related)
{
	related->predicted_better_aggregation++;
}

/***********************************************************************************************************
 * FUNCTIONS TO WRITE THE STATS FILE *
 ***********************************************************************************************************/
//the functions that write the stats file are organized in a way as to be used by both library and kernel module implementations. The kernel module implementation uses the proc interface. The functions iterate through statistics list.
//get_first points the first element (and locks the relevant mutex)
struct request_file_t *get_first(short int predicted_stats)
{
	struct agios_list_head *reqfile_l;
	struct request_file_t *req_file=NULL;

	while(!req_file)
	{
		if((current_scheduler->needs_hashtable) || (predicted_stats))
			hashtable_lock(hash_position);	
		reqfile_l = &hashlist[hash_position];

		if(!agios_list_empty(reqfile_l))
		{ //take the first element of the list
			agios_list_for_each_entry(req_file, reqfile_l, hashlist)
				break;
		}	
		else
		{//list is empty, nothing to show, unlock the mutex
			if((current_scheduler->needs_hashtable) || (predicted_stats))
			{
				hashtable_unlock(hash_position);
			}
			hash_position++;
			if(hash_position >= AGIOS_HASH_ENTRIES)
			{
				timeline_unlock();
				break;
			}
		}
	}
	return req_file;	
}

void print_something(const char *something)
{
#ifdef AGIOS_KERNEL_MODULE
	seq_printf(stats_file, 
#else
	fprintf(stats_file, 
#endif
		"%s", something);
}

void print_selected_algs(void)
{
	int i;

	print_something("Scheduling algorithms:\n");
	i = proc_algs_start;
	while(i != proc_algs_end)
	{
#ifdef AGIOS_KERNEL_MODULE
		seq_printf(stats_file, 
#else
		fprintf(stats_file, 
#endif
			"%lu\t%s\n", proc_algs_timestamps[i], get_algorithm_name_from_index(proc_algs[i]));
		i++;
		if(i >= config_agios_proc_algs)
			i =0;
	}
	print_something("\n");
}

//start prints the initial information in the file and calls the other functions
#ifdef AGIOS_KERNEL_MODULE
void *print_stats_start(struct seq_file *s, loff_t *pos)
#else
void print_stats_start(void)
#endif
{
#ifdef AGIOS_KERNEL_MODULE
	stats_file = s;
#endif
	print_selected_algs();
	
	print_something("\t\treqs\tsize\tsize_avg\tsize_min\tsize_max\tsum_/_aggs\tagg_bigg\tagg_last\tshift\tbetter\tpdt_better\ttbr_avg\ttbr_min\ttbr_max\tproc_req\tband\n");

#ifdef AGIOS_KERNEL_MODULE
	hash_position=0;

	if(!current_scheduler->needs_hashtable)
		timeline_lock();
	return get_first(0);
#endif
}

#ifdef AGIOS_KERNEL_MODULE
void *print_predicted_stats_start(struct seq_file *s, loff_t *pos)
#else
void print_predicted_stats_start(void)
#endif
{

#ifdef AGIOS_KERNEL_MODULE
	seq_printf(s, 
#else
	fprintf(stats_file, 
#endif
	"\nPREDICTED_STATISTICS\n\ntracefile_counter: %d\nprediction_thread_init_time: %llu\nprediction_time_accepted_error: %d\ncurrent_predicted_reqfilenb: %d\n", get_predict_tracefile_counter(), get_predict_init_time(), config_predict_agios_time_error, get_current_predicted_reqfilenb());
	
	print_something("\t\treqs\tsize\tsize_avg\tsize_min\tsize_max\tsum_/_aggs\tagg_bigg\ttbr_avg\ttbr_min\ttbr_max\n");

#ifdef AGIOS_KERNEL_MODULE
	hash_position=0;
	return get_first(1); 
#endif
}

void predicted_stats_show_related_list(struct related_list_t *related, const char *list_name)
{
	unsigned long int min_req_size, avg_req_size;
	float avg_agg_size;

	if(related->stats_file.min_req_size == ~0)
		min_req_size = 0;
	else
		min_req_size = related->stats_file.min_req_size;
	if(related->stats_file.processedreq_nb > 0)
		avg_req_size = (related->stats_file.total_req_size / related->stats_file.processedreq_nb);
	else
		avg_req_size = 0;
	if(related->stats_file.aggs_no > 0) 
		avg_agg_size = (((float)related->stats_file.sum_of_agg_reqs)/((float)related->stats_file.aggs_no));
	else
		avg_agg_size = 1;

	if(strcmp(list_name, "read") == 0)
	{
#ifdef AGIOS_KERNEL_MODULE
		seq_printf(stats_file, 
#else
		fprintf(stats_file, 
#endif
		"file id: %s\n", related->req_file->file_id);
	} 

#ifdef AGIOS_KERNEL_MODULE
	seq_printf(stats_file, 
#else
	fprintf(stats_file, 
#endif
	"\t%s:\t%lu\t%llu\t%lu\t%lu\t%lu\t%lu/%lu=%.2f\t%u\t%.2f\t%.2f\n",
	   list_name,
	   related->stats_file.processedreq_nb,
	   related->stats_file.total_req_size,
	   avg_req_size,
	   min_req_size,
	   related->stats_file.max_req_size,
	   related->stats_file.sum_of_agg_reqs,
	   related->stats_file.aggs_no,
	   avg_agg_size,
	   related->best_agg,
	   related->stats_file.avg_distance,
	   related->avg_stripe_difference);

}

#ifdef AGIOS_KERNEL_MODULE
static int predicted_stats_show_one(struct seq_file *s, void *v)
#else
void predicted_stats_show_one(struct request_file_t *req_file)
#endif
{
#ifdef AGIOS_KERNEL_MODULE
	struct request_file_t *req_file = (struct request_file_t *)v;
	stats_file = s;
#endif
	if((req_file->predicted_reads.stats_file.processedreq_nb + req_file->predicted_writes.stats_file.processedreq_nb) <= 0)
#ifndef AGIOS_KERNEL_MODULE
			return;
#else
			return 0;
#endif

	predicted_stats_show_related_list(&req_file->predicted_reads, "read");
	predicted_stats_show_related_list(&req_file->predicted_writes, "write");

#ifdef AGIOS_KERNEL_MODULE
	return 0;
#endif	
}

#ifndef AGIOS_KERNEL_MODULE
void stats_show_predicted(void)
{
	struct request_file_t *req_file;
	int i;
	struct agios_list_head *reqfile_l;

	for(i = 0; i < AGIOS_HASH_ENTRIES; i++)
	{
		reqfile_l = hashtable_lock(i);
		
		if(!agios_list_empty(reqfile_l))
		{
			agios_list_for_each_entry(req_file, reqfile_l, hashlist)
			{
				if((req_file->predicted_writes.stats_file.processedreq_nb > 0) || (req_file->predicted_reads.stats_file.processedreq_nb > 0))
					predicted_stats_show_one(req_file);
			}
		}		

		hashtable_unlock(i);
	}
}
#endif

void stats_show_related_list(struct related_list_t *related, const char *list_name)
{
	unsigned long int min_request_time;
	unsigned long int min_req_size, avg_req_size;
	float avg_agg_size;
	float bandwidth=0.0;
	float tmp_time;

	if(related->stats_file.min_request_time == ~0)
		min_request_time = 0;
	else
		min_request_time = related->stats_file.min_request_time;

	if(related->stats_file.min_req_size == ~0)
		min_req_size = 0;
	else
		min_req_size = related->stats_file.min_req_size;
	if(related->stats_file.receivedreq_nb > 0)
		avg_req_size = (related->stats_file.total_req_size / related->stats_file.receivedreq_nb);
	else
		avg_req_size = 0;
	if(related->stats_file.aggs_no > 0) 
		avg_agg_size = (((float)related->stats_file.sum_of_agg_reqs)/((float)related->stats_file.aggs_no));
	else
		avg_agg_size = 0;
	if(related->stats_file.processedreq_nb > 0)
	{
		tmp_time = get_ns2s(related->stats_file.processed_req_time);
		if(tmp_time > 0)		
			bandwidth = ((related->stats_file.processed_req_size) / tmp_time) / 1024.0;
	}

	if(strcmp(list_name, "read") == 0)
	{
#ifndef AGIOS_KERNEL_MODULE
		fprintf(stats_file, 
#else	
		seq_printf(stats_file,
#endif
	   	"file id: %s\n", related->req_file->file_id);
	}

#ifndef AGIOS_KERNEL_MODULE
	fprintf(stats_file, 
#else
	seq_printf(stats_file,
#endif
	   "\t%s:\t%lu\t%llu\t%lu\t%lu\t%lu\t%lu/%lu=%.2f\t%u\t%u\t%lu\t%lu\t%lu\t%.2f\t%lu\t%lu\t%lu\t%.2f\n",
	   list_name,
	   related->stats_file.receivedreq_nb,
	   related->stats_file.total_req_size,
	   avg_req_size,
	   min_req_size,
	   related->stats_file.max_req_size,
	   related->stats_file.sum_of_agg_reqs,
	   related->stats_file.aggs_no,
	   avg_agg_size,
	   related->best_agg,
	   related->lastaggregation,
	   related->shift_phenomena,
	   related->better_aggregation,
	   related->predicted_better_aggregation,
           related->stats_file.avg_request_time,
	   min_request_time,
	   related->stats_file.max_request_time,
	   related->stats_file.processedreq_nb,
	   bandwidth);

}

#ifdef AGIOS_KERNEL_MODULE
static int stats_show_one(struct seq_file *s, void *v)
#else
void stats_show_one(struct request_file_t *req_file)
#endif
{
#ifdef AGIOS_KERNEL_MODULE
	struct request_file_t *req_file = (struct request_file_t *)v;
	stats_file = s;
#endif
	if((req_file->related_reads.stats_file.receivedreq_nb + req_file->related_writes.stats_file.receivedreq_nb) <= 0)
#ifndef AGIOS_KERNEL_MODULE
			return;
#else
			return 0;
#endif
	stats_show_related_list(&req_file->related_reads, "read");
	stats_show_related_list(&req_file->related_writes, "write");

#ifdef AGIOS_KERNEL_MODULE
	return 0;
#endif
}

#ifdef AGIOS_KERNEL_MODULE
static void stats_show_ending(struct seq_file *s, void *v)
#else
void stats_show_ending(void)
#endif
{
	double *bandwidth;
	int i, len;
	char *band_str = malloc(sizeof(char)*50*config_agios_performance_values);

	agios_mutex_lock(&global_statistics_mutex);

#ifndef AGIOS_KERNEL_MODULE
	fprintf(stats_file,  
#else
	seq_printf(stats_file,
#endif
	"total of %lu requests\n\tavg\tmin\tmax\nglobal time between requests:\t%.2f\t%lu\t%lu\nrequest size:\t%.2f\t%lu\t%lu\n", 
	stats_for_file.total_reqnb, 
	stats_for_file.avg_req_time,
	stats_for_file.min_req_time,
	stats_for_file.max_req_time,
	stats_for_file.avg_req_size,
	stats_for_file.min_req_size,
	stats_for_file.max_req_size);

	agios_mutex_unlock(&global_statistics_mutex);

	bandwidth = agios_get_performance_bandwidth();
	band_str[0]='\n';
	band_str[1]='\0';
	i = performance_start;
	while(i != performance_end)
	{
		len = strlen(band_str);
		sprintf(band_str+len, "%.2f\n", bandwidth[i]/1024.0);
		i++;
		if(i >= config_agios_performance_values)
			i = 0;
	}
#ifndef AGIOS_KERNEL_MODULE
	fprintf(stats_file,  
#else
	seq_printf(stats_file,
#endif
	"served an amount of %llu bytes in a total of %llu ns, bandwidth was:%s",
	agios_get_performance_size(),
	agios_get_performance_time(),
	band_str);


}

#ifndef AGIOS_KERNEL_MODULE
void stats_show(void)
{
	struct request_file_t *req_file;
	int i;
	struct agios_list_head *reqfile_l;

	if(!current_scheduler->needs_hashtable)
		timeline_lock();

	for(i = 0; i < AGIOS_HASH_ENTRIES; i++)
	{
		if(current_scheduler->needs_hashtable)
			reqfile_l = hashtable_lock(i);
		else
			reqfile_l = &hashlist[i];
			
		if(!agios_list_empty(reqfile_l))
		{
			agios_list_for_each_entry(req_file, reqfile_l, hashlist)
			{
				stats_show_one(req_file);
			}
		}		
		if(current_scheduler->needs_hashtable)	
			hashtable_unlock(i);
	}

	if(!current_scheduler->needs_hashtable)
		timeline_unlock();

	stats_show_ending();
}
#endif

void reset_stats_related_list(struct related_list_t *related_list)
{
	related_list->laststartoff = 0;
	related_list->lastfinaloff = 0;
	related_list->predictedoff = 0;

	related_list->lastaggregation = 0;
	related_list->best_agg = 0;

	related_list->shift_phenomena = 0;
	related_list->better_aggregation = 0;
	related_list->predicted_better_aggregation = 0;

	related_list->stats_file.processedreq_nb = 0;
	related_list->stats_file.receivedreq_nb = 0;
	related_list->stats_file.processed_req_size = 0;
	related_list->stats_file.processed_req_time = 0;

	related_list->stats_file.total_req_size=0;
	related_list->stats_file.min_req_size=~0;
	related_list->stats_file.max_req_size=0;

	related_list->stats_file.max_request_time = 0;
	related_list->stats_file.avg_request_time = 0.0;
	related_list->stats_file.min_request_time = ~0;

	related_list->stats_file.avg_distance=0.0;

	related_list->stats_file.aggs_no = 0;
	related_list->stats_file.sum_of_agg_reqs = 0;

}

void agios_reset_stats()
{
	struct request_file_t *req_file;
	int i;
	struct agios_list_head *reqfile_l;

	reset_global_reqstats_file();
#ifndef AGIOS_KERNEL_MODULE
	if(config_trace_agios)
		agios_trace_reset();	
#endif

	if(!current_scheduler->needs_hashtable)
		timeline_lock();

	for(i = 0; i < AGIOS_HASH_ENTRIES; i++)
	{
		if(current_scheduler->needs_hashtable)
			hashtable_lock(i);
		reqfile_l = &hashlist[i];
		
		if(!agios_list_empty(reqfile_l))
		{
			agios_list_for_each_entry(req_file, reqfile_l, hashlist)
			{
				reset_stats_related_list(&req_file->related_reads);
				reset_stats_related_list(&req_file->related_writes);

			}
		}
		if(current_scheduler->needs_hashtable)
			hashtable_unlock(i);
	}
	if(!current_scheduler->needs_hashtable)
		timeline_unlock();

}

#ifndef AGIOS_KERNEL_MODULE
void agios_print_stats_file(char *filename)
{
	PRINT_FUNCTION_NAME;

	/*opens the stats file*/
	if(!filename)
	{
		agios_print("AGIOS: No filename to stats file!\n");
		return;
	}
	stats_file = fopen(filename, "w"); 
	if(!stats_file)
	{
		agios_print("Cannot create %s stats file\n", filename);
		return;
	}
	else
	{
		/*we can't show stats if the prediction thread did not finished reading from trace files*/
		agios_wait_predict_init();
		/*prints the start of the file*/	
		print_stats_start();
	
		/*prints the stats*/	
		stats_show();

		print_predicted_stats_start();
		stats_show_predicted();	

		/*closes the file*/
		fclose(stats_file);
	}

}
#endif

/*register proc entries (useful only to the kernel module implementation) and some other structures*/
void proc_stats_init(void)
{
	proc_algs = agios_alloc(sizeof(int)*(config_agios_proc_algs+1));
	proc_algs_timestamps = agios_alloc(sizeof(unsigned long int)*(config_agios_proc_algs+1));
	proc_algs_start = proc_algs_end = 0;
	
#ifdef AGIOS_KERNEL_MODULE
	struct proc_dir_entry *entry;

	agios_proc_dir = proc_mkdir("agios", NULL);
	if(!agios_proc_dir)
	{
		agios_print("Cannot create /proc/agios entry");
		return;
	}	
	
	entry = create_proc_entry("stats", 444, agios_proc_dir);
	if(entry)
		entry->proc_fops = &stats_proc_ops;

	entry = create_proc_entry("predicted_stats", 444, agios_proc_dir);
	if(entry)
		entry->proc_fops = &predicted_stats_proc_ops;

#ifdef AGIOS_DEBUG
	entry = create_proc_entry("hashtable", 444, agios_proc_dir);
	if(entry)
		entry->proc_fops = &hashtable_proc_ops;
#endif
			
#endif
}
/*unregister proc entries (useful only to the kernel module implementation) and free data structures*/
void proc_stats_exit(void)
{
#ifdef AGIOS_KERNEL_MODULE
	remove_proc_entry("stats", agios_proc_dir);
	remove_proc_entry("predicted_stats", agios_proc_dir);
#ifdef AGIOS_DEBUG
	remove_proc_entry("hashtable", agios_proc_dir);
#endif
	remove_proc_entry("agios", NULL);	
#endif
	free(proc_algs);
	free(proc_algs_timestamps);
	if(last_req)
		free(last_req);
	if(last_read_req)
		free(last_read_req);
	if(last_write_req)
		free(last_write_req);
}
void fill_queue_stats(struct agios_queue_statistics_t *new, struct queue_statistics_t *stats)
{
	float this_time; 
	new->processedreq_nb = stats->processedreq_nb;
	new->receivedreq_nb = stats->receivedreq_nb;
	new->processed_size = stats->processed_req_size;
	//convert ns to s
	this_time= ((float)stats->processed_req_time)/1000.0;
	this_time = this_time/1000.0;
	this_time = this_time/1000.0;
	if((stats->processedreq_nb > 0) && (this_time > 0))
		new->bandwidth = (((float)stats->processed_req_size)/1024.0)/((float)this_time);
	else
		new->bandwidth = 0.0;
	new->received_size = stats->total_req_size;
	new->avg_req_size = ((float)stats->total_req_size)/((float)stats->receivedreq_nb); //here we know receivedreq is greater than 0 because we tested it before calling the function
	new->max_req_size = stats->max_req_size ;
	if(stats->receivedreq_nb > 0)
		new->min_req_size = stats->min_req_size ;
	else
		new->min_req_size = 0;
	new->avg_request_time = stats->avg_request_time/1000.0 ;
	new->max_request_time = stats->max_request_time/1000 ;
	if(stats->receivedreq_nb > 1)
		new->min_request_time = stats->min_request_time/1000 ;
	else
		new->min_request_time = 0;
	new->avg_distance = stats->avg_distance ;
}
struct agios_statistics_t *agios_get_statistics_and_reset(void)
{
	struct request_file_t *req_file;
	struct agios_list_head *reqfile_l;
	int i;
	struct agios_statistics_t *ret;
	struct agios_file_statistics_t *last;
	int current_file;

	ret = malloc(sizeof(struct agios_statistics_t));
	if(!ret)
	{
		agios_print("PANIC! Could not allocate memory for reporting statistics!");
		ret = NULL;
	}
	else
	{
		//fill global statistics
		agios_mutex_lock(&global_statistics_mutex);

		ret->global.total_reqnb = stats_for_window.total_reqnb;
		ret->global.total_readnb = stats_for_window.total_readnb;
		ret->global.total_writenb = stats_for_window.total_writenb;
		ret->global.avg_req_time = stats_for_window.avg_req_time/1000.0;
		ret->global.max_req_time = stats_for_window.max_req_time/1000;
		if(stats_for_window.total_reqnb>1)
			ret->global.min_req_time = stats_for_window.min_req_time/1000;
		else
			ret->global.min_req_time = 0;

		ret->global.avg_read_req_time = stats_for_window.avg_read_req_time/1000.0;
		ret->global.max_read_req_time = stats_for_window.max_read_req_time/1000.0;
		if(stats_for_window.total_readnb > 1)
			ret->global.min_read_req_time = stats_for_window.min_read_req_time/1000;
		else
			ret->global.min_read_req_time = 0;
		ret->global.avg_write_req_time = stats_for_window.avg_write_req_time/1000.0;
		ret->global.max_write_req_time = stats_for_window.max_write_req_time/1000;
		if(stats_for_window.total_writenb > 1)
			ret->global.min_write_req_time = stats_for_window.min_write_req_time/1000;
		else
			ret->global.min_write_req_time = 0;
		ret->global.avg_req_size = stats_for_window.avg_req_size;
		ret->global.max_req_size = stats_for_window.max_req_size;
		if(stats_for_window.total_reqnb > 0)
			ret->global.min_req_size = stats_for_window.min_req_size;
		else
			ret->global.min_req_size = 0;
		ret->global.avg_read_req_size = stats_for_window.avg_read_req_size;
		ret->global.max_read_req_size = stats_for_window.max_read_req_size;
		if(stats_for_window.total_readnb > 0)
			ret->global.min_read_req_size = stats_for_window.min_read_req_size;
		else
			ret->global.min_read_req_size = 0;
		ret->global.avg_write_req_size = stats_for_window.avg_write_req_size;
		ret->global.max_write_req_size = stats_for_window.max_write_req_size;
		if(stats_for_window.total_writenb > 0)
			ret->global.min_write_req_size = stats_for_window.min_write_req_size;
		else
			ret->global.min_write_req_size = 0; 
		
		_reset_global_reqstats(&stats_for_window);

		agios_mutex_unlock(&global_statistics_mutex);

		//fill local statistics
		last = NULL;
		ret->files = NULL;

		//find out the number of files
		if(!current_scheduler->needs_hashtable)
			timeline_lock();

		ret->filenb = 0;
		for(i = 0; (i < AGIOS_HASH_ENTRIES); i++)
		{		
			if(current_scheduler->needs_hashtable)
				hashtable_lock(i);

			reqfile_l = &hashlist[i];
			if(!agios_list_empty(reqfile_l))
			{
				agios_list_for_each_entry(req_file, reqfile_l, hashlist)
				{
					if(req_file->stats_window.counter > 1) 
						ret->filenb+=1;
				}
			}
		}
		if(ret->filenb < 0) //overflow?
			ret->filenb = INT_MAX;
		if(ret->filenb > 0)
		{
			ret->files = malloc(sizeof(struct agios_file_statistics_t)*ret->filenb);
			if(!(ret->files))
			{
				agios_print("PANIC! Could not allocate memory for reporting statistics!");
			}
		}
		else
			ret->files = NULL;

		current_file = 0;
		for(i = 0; (i < AGIOS_HASH_ENTRIES); i++)
		{
			reqfile_l = &hashlist[i];
		
			if(!agios_list_empty(reqfile_l)) 
			{
				agios_list_for_each_entry(req_file, reqfile_l, hashlist)
				{
					if(/*(req_file->stats_window.counter > 1) &&*/ (ret->files != NULL) && (current_file < ret->filenb)) //we don't report on files that did not receive at least two requests (otherwise it makes no sense to talk about time between requests and offset distance)//we test ret->files in case we were not able to allocate memory (then we won't copy anything, just reset stuff and unlock the mutexes). 
					{
						last = &(ret->files[current_file]);
						current_file++;
						
						//copy fields
						strncpy(last->file_id, req_file->file_id, 100);
						last->file_id[99]='\0';

						last->avg_request_time = req_file->stats_window.avg_request_time/1000.0;
						last->max_request_time = req_file->stats_window.max_request_time/1000;
						if(req_file->stats_window.counter > 1)
							last->min_request_time = req_file->stats_window.min_request_time/1000;
						else
							last->min_request_time = 0;
						last->avg_distance = req_file->stats_window.avg_distance;
						last->read_stats = 0;
						last->write_stats = 0;

						//fill statistics per queue
						if(req_file->related_reads.stats_window.receivedreq_nb > 1) //we only report queues that received at least two requests
						{
							last->read_stats= 1;
							fill_queue_stats(&(last->read), &(req_file->related_reads.stats_window));
						}	
						if(req_file->related_writes.stats_window.receivedreq_nb > 1) 
						{
							last->write_stats = 1;
							fill_queue_stats(&(last->write), &(req_file->related_writes.stats_window));
						}	
					} //end if counter > 1
				
					//reset stats	
					reset_stats_window_related_list(&req_file->related_reads);
					reset_stats_window_related_list(&req_file->related_writes);
					reset_stats_window_file(req_file);
				} //end for all files in the line
			} //end if list not empty
			if(current_scheduler->needs_hashtable)
				hashtable_unlock(i);
		} //end for hash entries
		if(!current_scheduler->needs_hashtable)
			timeline_unlock();
		if((ret->files == NULL) && (ret->filenb > 0)) //we could not allocate memory
		{
			agios_free_statistics_t(&ret);
		}
	} //end else (ret is not NULL)
	return ret;
}

void agios_free_statistics_t(struct agios_statistics_t **stats)
{
	if(*stats)
	{
		if((*stats)->files)
			free((*stats)->files);
		free(*stats);
		*stats = NULL;
	}
}

int compare(const void *a, const void *b)
{
    return (*(int *) a - *(int *) b);
}

float pending_size = 0;

void prepare_twins_metrics(char *fd)
{
	struct request_file_t *req_file;
	struct agios_list_head *reqfile_l;
	int i;

	unsigned long int file_handles;

	unsigned long int total_processed_request_size;

	unsigned long int read_received_request_number, write_received_request_number;
	unsigned long int read_processed_request_number, write_processed_request_number;

	float read_total_received_size, write_total_received_size;
	float read_total_distance, write_total_distance;

	float read_count_received_size, write_count_received_size;
	float read_count_distance, write_count_distance;

	float read_avg_received_size, write_avg_received_size;
	unsigned long int read_min_request_size, write_min_request_size;
	unsigned long int read_max_request_size, write_max_request_size;
	float read_avg_distance, write_avg_distance;
	
	float processed_bandwidth;
	float total_input_request_size;

	// we need a single string message, split with ';' with the following metrics
	//  0 pvfs_servers
	//  1 clients
	//  2 processes
	//  3 operation
	//  4 file_handles
	//  5 received_request_number
	//  6 avg_received_size
	//  7 max_request_size
	//  8 avg_distance
	//  9 bandwidth (feedback)
	// time is reported in us, sizes in bytes, bandwidth in KB per second

	// NOTICE -----------------------------------------------------------------
	// in this initial version we are not considering mixed workload, therefore
	// we only report the values for the operation that occurred more times
	// ------------------------------------------------------------------------

	read_total_distance = 0.0;
	write_total_distance = 0.0;

	read_count_distance = 0;
	write_count_distance = 0;

	read_count_received_size = 0;
	write_count_received_size = 0;

	file_handles = 0;

	read_received_request_number = 0;
	read_avg_received_size = 0.0;
	read_min_request_size = ULONG_MAX;
	read_max_request_size = 0;

	total_processed_request_size = 0;
	total_input_request_size = 0;

	write_received_request_number = 0;
	write_avg_received_size = 0.0;
	write_min_request_size = ULONG_MAX;
	write_max_request_size = 0;

	// write local statistics
	if (!current_scheduler->needs_hashtable)
		timeline_lock();

	for (i = 0; (i < AGIOS_HASH_ENTRIES); i++)
	{		
		if (current_scheduler->needs_hashtable)
			hashtable_lock(i);

		reqfile_l = &hashlist[i];
		if (!agios_list_empty(reqfile_l))
		{
			agios_list_for_each_entry(req_file, reqfile_l, hashlist)
			{
				// count the number of file handles reported
				file_handles = file_handles + 1;

				// REVIEW (sum read and writes)
				// increament the number of requests
				read_received_request_number = read_received_request_number + req_file->related_reads.stats_window.receivedreq_nb;
				write_received_request_number = write_received_request_number + req_file->related_writes.stats_window.receivedreq_nb;

				// REVIEW (sum read and writes)
				// increment the number of processed requests
				read_processed_request_number = read_processed_request_number + req_file->related_reads.stats_window.processedreq_nb;
				write_processed_request_number = write_processed_request_number + req_file->related_writes.stats_window.processedreq_nb;

				// REVIEW (min between read and writes as well)
				// computes the minimum request size
				if (req_file->related_reads.stats_window.min_req_size < read_min_request_size)
					read_min_request_size = req_file->related_reads.stats_window.min_req_size;

				if (req_file->related_writes.stats_window.min_req_size < write_min_request_size)
					write_min_request_size = req_file->related_writes.stats_window.min_req_size;

				// REVIEW (max between read and writes as well)
				// computes the maximum request size
				if (req_file->related_reads.stats_window.max_req_size > read_max_request_size)
					read_max_request_size = req_file->related_reads.stats_window.max_req_size;

				if (req_file->related_writes.stats_window.max_req_size > write_max_request_size)
					write_max_request_size = req_file->related_writes.stats_window.max_req_size;
				
				//agios_print("R: %.10lf / %.10lf", ((float)req_file->related_reads.stats_window.total_req_size)/((float)req_file->related_reads.stats_window.receivedreq_nb));
				//read_total_received_size += ((req_file->related_reads.stats_window.receivedreq_nb > 0) ? ((float)req_file->related_reads.stats_window.total_req_size)/((float)req_file->related_reads.stats_window.receivedreq_nb) : 0.0);			
				//read_count_received_size++;

				read_total_received_size += (float)req_file->related_reads.stats_window.total_req_size;
				read_count_received_size += (float)req_file->related_reads.stats_window.receivedreq_nb;

				//agios_print("W: %.10lf / %.10lf", ((float)req_file->related_writes.stats_window.total_req_size)/((float)req_file->related_writes.stats_window.receivedreq_nb));
				//write_total_received_size += ((req_file->related_writes.stats_window.receivedreq_nb > 0) ? ((float)req_file->related_writes.stats_window.total_req_size)/((float)req_file->related_writes.stats_window.receivedreq_nb) : 0.0);
				//write_count_received_size++;

				write_total_received_size += (float)req_file->related_writes.stats_window.total_req_size;
				write_count_received_size += (float)req_file->related_writes.stats_window.receivedreq_nb;
		
				// OPTION 2
				// pending_size += (float)req_file->related_reads.stats_window.total_req_size + (float)req_file->related_writes.stats_window.total_req_size;

				// OPTION 3
				total_input_request_size += (float)req_file->related_reads.stats_window.total_req_size + (float)req_file->related_writes.stats_window.total_req_size;

				read_total_distance += req_file->stats_window.avg_distance;
				read_count_distance++;

				write_total_distance += req_file->stats_window.avg_distance;
				write_count_distance++;

				// REVIEW
				// total request size to compute the bandwidth (general, not per operation)
				total_processed_request_size = total_processed_request_size + req_file->related_reads.stats_window.processed_req_size + req_file->related_writes.stats_window.processed_req_size;

				// reset stats (we do not do that in here anymore)
				//reset_stats_window_related_list(&req_file->related_reads);
				//reset_stats_window_related_list(&req_file->related_writes);
				//reset_stats_window_file(req_file);
			} // end for all files in the line
		} // end if list not empty

		if (current_scheduler->needs_hashtable)
			hashtable_unlock(i);
	} // end for hash entries

	if (!current_scheduler->needs_hashtable)
		timeline_unlock();

	// NOTICE -----------------------------------------------------------------
	// in this initial version we are not considering mixed workload, therefore
	// we only report the values for the operation that occurred more times
	// ------------------------------------------------------------------------

	// OPTION - 1
	// Compute the general bandwidth based on the processed and the observation window
	//processed_bandwidth = total_processed_request_size / (config_dynamic_twins_period / 1000.0); // KB/s (period is in us)

	// OPTION - 2
	// We will try to use another reward: the percentage of the requests we have processed compared to what we have received
	/*if (pending_size == 0) {
		processed_bandwidth = 0.0;
	} else {
		processed_bandwidth = total_processed_request_size / pending_size * 100;
		pending_size -= total_processed_request_size;
	}*/

	// OPTION - 3
	/*if (total_input_request_size == 0) {
		processed_bandwidth = 0;
	} else {
		processed_bandwidth = total_processed_request_size / total_input_request_size;
	}*/

	// OPTION - 4 (correct)
	processed_bandwidth = total_processed_request_size / 1024.0 / 1024.0;

	/*metrics->app_timeline_size = app_timeline_size;
	metrics->config_dynamic_twins_clients = config_dynamic_twins_clients;
	metrics->config_dynamic_twins_processes = config_dynamic_twins_processes;
	metrics->file_handles = file_handles;*/

	if (read_received_request_number > write_received_request_number) {
		// Compute the average (not the median as before) received request size
		read_avg_received_size = (read_count_received_size > 0) ? read_total_received_size / read_count_received_size : 0.0;

		// Compute the average (not the median as before) offset distance
		read_avg_distance = (read_count_distance > 0) ? read_total_distance / read_count_distance : 0.0;
		
		sprintf(fd, "%u;%u;%u;%u;%lu;%.3lf;%lu;%lu;%.3f;%.10f\n",
			app_timeline_size,
			config_dynamic_twins_clients,
			config_dynamic_twins_processes,
			TWINS_NN_MODEL_READ,
			file_handles,
			read_avg_received_size,
			(read_min_request_size == ULONG_MAX ? 0 : read_min_request_size),
			read_max_request_size,
			read_avg_distance,
			processed_bandwidth
		);

		/*metrics->operation = TWINS_NN_MODEL_READ;
		if (read_count_received_size > 0) {
			metrics->avg_received_size = read_avg_received_size;
			// When we collected metrics, in this case, 0.0 was reported, we need to make sure we do it here
			if (read_min_request_size == ULONG_MAX) {
				read_min_request_size = 0;
			}
			metrics->min_request_size = read_min_request_size;
			metrics->max_request_size = read_max_request_size;
			metrics->avg_distance = read_avg_distance;
		} else {
			metrics->avg_received_size = 0;
			metrics->min_request_size = 0;
			metrics->max_request_size = 0;
			metrics->avg_distance = 0;
		}
		metrics->reward = processed_bandwidth; 
		*/
	} else {
		// Compute the average (not the median as before) received request size
		write_avg_received_size = (write_count_received_size > 0) ? write_total_received_size / write_count_received_size : 0.0;

		// Compute the average (not the median as before) offset distance
		write_avg_distance = (write_count_distance > 0) ? write_total_distance / write_count_distance : 0.0;

		sprintf(fd, "%u;%u;%u;%u;%lu;%.3lf;%lu;%lu;%.3f;%.10f\n",
			app_timeline_size,
			config_dynamic_twins_clients,
			config_dynamic_twins_processes,
			TWINS_NN_MODEL_WRITE,
			file_handles,
			write_avg_received_size,
			(write_min_request_size == ULONG_MAX ? 0 : write_min_request_size),
			write_max_request_size,
			write_avg_distance,
			processed_bandwidth
		);

		/*metrics->operation = TWINS_NN_MODEL_WRITE;
		if (write_count_received_size > 0) {
			metrics->avg_received_size = write_avg_received_size;
			// When we collected metrics, in this case, 0.0 was reported, we need to make sure we do it here
			if (write_min_request_size == ULONG_MAX) {
				write_min_request_size = 0;
			}
			metrics->min_request_size = write_min_request_size;
			metrics->max_request_size = write_max_request_size;
			metrics->avg_distance = write_avg_distance;
		} else {
			metrics->avg_received_size = 0;
			metrics->min_request_size = 0;
			metrics->max_request_size = 0;
			metrics->avg_distance = 0;
		}
		metrics->reward = processed_bandwidth; 
		*/
	}

	// debug("message: %s", fd);

	return;
}

void reset_twins_metrics(void)
{
	struct request_file_t *req_file;
	struct agios_list_head *reqfile_l;
	int i;

	// write local statistics
	if (!current_scheduler->needs_hashtable)
		timeline_lock();

	for (i = 0; (i < AGIOS_HASH_ENTRIES); i++)
	{		
		if (current_scheduler->needs_hashtable)
			hashtable_lock(i);

		reqfile_l = &hashlist[i];
		if (!agios_list_empty(reqfile_l))
		{
			agios_list_for_each_entry(req_file, reqfile_l, hashlist)
			{
				// reset TWINS metrics stats	
				reset_stats_window_related_list(&req_file->related_reads);
				reset_stats_window_related_list(&req_file->related_writes);
				reset_stats_window_file(req_file);
			}
		}

		if (current_scheduler->needs_hashtable)
			hashtable_unlock(i);
	} // end for hash entries

	if (!current_scheduler->needs_hashtable)
		timeline_unlock();
}

/***********************************************************************************
 * functions to handle the proc entry - for the kernel module implementation only  *
 ***********************************************************************************/
#ifdef AGIOS_KERNEL_MODULE
static void *stats_seq_next(struct seq_file *s, void *v, loff_t *pos) 
{
	struct request_file_t *req_file;
	
	(*pos)++;
	if (*pos >= current_reqfilenb) 
		return NULL;
	
	req_file = (struct request_file_t *)v;

	if(req_file->hashlist.next == &hashlist[hash_position]) //we finished printing this hash position
	{
		if(current_scheduler->needs_hashtable)
			hashtable_unlock(hash_position);

		hash_position++;
		if(hash_position >= AGIOS_HASH_ENTRIES) //we are done with the whole hashtable
		{
			if(!current_scheduler->needs_hashtable)
				timeline_unlock();
			return NULL;
		}
		else
			return get_first(0);
		}
	}
	return agios_list_entry(req_file->hashlist.next, struct request_file_t, hashlist);
}
static void *predicted_stats_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
	struct request_file_t *req_file;
	
	(*pos)++;
	if (*pos >= get_current_predicted_reqfilenb()) 
		return NULL;
	
	req_file = (struct request_file_t *)v;

	if(req_file->hashlist.next == &hashlist[hash_position]) //we finished printing this hash position
	{
		hashtable_unlock(hash_position);
		hash_position++;
		if(hash_position >= AGIOS_HASH_ENTRIES) //we are done with the whole hashtable
			return NULL;
		else
			return get_first(1);
	}
	return agios_list_entry(req_file->hashlist.next, struct request_file_t, hashlist);
}

static struct seq_operations stats_seq_ops = {
	.start		= print_stats_start,
	.next		= stats_seq_next,
	.stop		= stats_show_ending,
	.show		= stats_show_one
};

static int stats_proc_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &stats_seq_ops);
}

static ssize_t stats_proc_write(struct file * file, const char __user * buf,
				    size_t count, loff_t *ppos)
{

	agios_reset_stats();
	return count;
}

static struct file_operations stats_proc_ops = {
	.owner		= THIS_MODULE,
	.open		= stats_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
	.write		= stats_proc_write,
};


static void predicted_stats_stop(struct seq_file *s, void *v) 
{
}

static struct seq_operations predicted_stats_seq_ops = {
	.start		= print_predicted_stats_start,
	.next		= predicted_stats_seq_next,
	.stop		= predicted_stats_stop,
	.show		= predicted_stats_show_one
};

static int predicted_stats_proc_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &predicted_stats_seq_ops);
}

static ssize_t predicted_stats_proc_write(struct file * file, const char __user * buf,
				    size_t count, loff_t *ppos)
{
	return count;
}

static struct file_operations predicted_stats_proc_ops = {
	.owner		= THIS_MODULE,
	.open		= predicted_stats_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
	.write		= predicted_stats_proc_write,
};

#ifdef AGIOS_DEBUG
static int hashtable_proc_index=0;
void *hashtable_start(struct seq_file *s, loff_t *pos) 
{
	hashtable_proc_index=0;
	if(current_scheduler->needs_hashtable)
		hashtable_lock(hashtable_proc_index);
	else
		timeline_lock();
	return &hashlist[hashtable_proc_index];
}

static int hashtable_show(struct seq_file *s, void *v) 
{
	struct request_file_t *req_file;
	struct agios_list_head *hash_list = (struct agios_list_head *) v;
	struct request_t *tmp;

	seq_printf(s, "[%d]\n", hashtable_proc_index);
	
	if(agios_list_empty(hash_list))
		return 0;

	agios_list_for_each_entry(req_file, hash_list, hashlist) {
		seq_printf(s, "     [%s]:", req_file->file_id);
		seq_printf(s, "\n\t reads : ");
		agios_list_for_each_entry(tmp, &req_file->related_reads.list, related) {
			seq_printf(s, "(%d; %d), ", (int)tmp->io_data.offset,
			                   	    (int)tmp->io_data.offset +
	   			        	    (int)tmp->io_data.len);
		}
		seq_printf(s, "\n\t writes: ");

		agios_list_for_each_entry(tmp, &req_file->related_writes.list, related) {
			seq_printf(s, "(%d; %d), ", (int)tmp->io_data.offset,
			       			    (int)tmp->io_data.offset +
			       			    (int)tmp->io_data.len);
		}
		seq_printf(s, "\n");
	}
	if(current_scheduler->needs_hashtable)
		hashtable_unlock(hashtable_proc_index);	
	else
		timeline_unlock();
	return 0;
}

static void *hashtable_seq_next(struct seq_file *s, void *v, loff_t *pos) 
{
	if(current_scheduler->needs_hashtable)
	{
		hashtable_proc_index++;
		return hashtable_lock(hashtable_proc_index);
	}
	else
		return NULL;
}

static void hashtable_stop(struct seq_file *s, void *v)
{
	hashtable_proc_index=0;
}

static struct seq_operations hashtable_seq_ops = {
	.start		= hashtable_start,
	.next		= hashtable_seq_next,
	.stop		= hashtable_stop,
	.show		= hashtable_show
};

static int hashtable_proc_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &hashtable_seq_ops);
}

static ssize_t hashtable_proc_write(struct file * file, const char __user * buf,
				    size_t count, loff_t *ppos)
{
	return count;
}

static struct file_operations hashtable_proc_ops = {
	.owner		= THIS_MODULE,
	.open		= hashtable_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
	.write		= hashtable_proc_write,
};

#endif /* #ifdef AGIOS_DEBUG */


#endif /* #ifdef AGIOS_KERNEL_MODULE */



