#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <agios.h>
#include <string.h>


#define REQ_TYPE 0
#define SLEEP 100

static int processed_reqnb=0;
static pthread_mutex_t processed_reqnb_mutex=PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t processed_reqnb_cond=PTHREAD_COND_INITIALIZER;

static int reqnb; 
static int req_size;
static int time_between;
static int offset_dist;
static int filesnb;

struct req_t
{
	int id;
	int fileid;
	long int offset;
};
static struct req_t *requests;
struct file_t
{
	char *filename;
	long int offset;
};
static struct file_t *files;

struct client clnt;

static pthread_t *processing_threads;
static int processing_threads_index;

char *filename;

struct agios_statistics_t *stats;


void inc_processed_reqnb()
{
	pthread_mutex_lock(&processed_reqnb_mutex);
	processed_reqnb++;
	if(processed_reqnb >= reqnb)
		pthread_cond_signal(&processed_reqnb_cond);
	pthread_mutex_unlock(&processed_reqnb_mutex);
}

void *process_request_thr(void *arg)
{
	struct req_t *req = (struct req_t *)arg;
	struct timespec sleep_time_tsp;

	printf("TEST\tthread created to process request starting, our request is %d offset %ld size %d file %s\n", req->id, req->offset, req_size, files[req->fileid].filename);

	sleep_time_tsp.tv_sec = SLEEP / 1000000000L;
	sleep_time_tsp.tv_nsec = SLEEP % 1000000000L;
	nanosleep(&sleep_time_tsp, NULL);

	agios_release_request(files[req->fileid].filename, REQ_TYPE, req_size, req->offset);

	inc_processed_reqnb();
}

void test_process(void * arg)
{
	struct req_t *req = (struct req_t *)arg;
	int ret;

	printf("TEST\tstarting thread to process request to be processed is %d offset %ld file %s\n", req->id, req->offset, files[req->fileid].filename);

	if(processing_threads_index >= reqnb)
	{
		printf("TEST\tPANIC! we received more requests than generated\n");
		agios_exit();
		exit(0);	
	}
	ret = pthread_create(&processing_threads[processing_threads_index], NULL, process_request_thr, req);		
	processing_threads_index++;
	if(ret != 0)
	{
		printf("TEST\tPANIC! Unable to create thread to process request!\n");
	}
}

int main (int argc, char **argv)
{
	int i, ret;
	struct timespec t1, t2;
	unsigned long long int elapsed;
	struct timespec timeout;
	struct agios_file_statistics_t *aux=NULL;
	short int alloc_problem=0;

	srand(1512);
	/*get arguments*/
	if(argc < 6)
	{
		printf("Usage: ./%s <number of requests> <requests' size in bytes> <time between requests in ns> <offset distance between requests> <number of files>\n", argv[0]);
		exit(1);
	}
	reqnb = atoi(argv[1]);
	req_size = atoi(argv[2]);
	time_between = atoi(argv[3]);
	offset_dist = atoi(argv[4]);
	filesnb = atoi(argv[5]);
	if((reqnb <=0) || (req_size <= 0) || (time_between < 0) || (offset_dist < 0) || (filesnb <= 0))
	{
		printf("Invalid test!\nUsage: ./%s <number of requests> <requests' size in bytes> <time between requests in ns> <offset distance between requests> <number of files>\n", argv[0]);
		exit(1);
		
	}
	printf("TEST\tGenerating %d requests to %d files of %d bytes every %dns with offset distance of %d bytes\n", reqnb, filesnb, req_size, time_between, offset_dist);

	/*start AGIOS*/
	clnt.process_requests = NULL;
	clnt.process_request = test_process;
	if(agios_init(&clnt, "/tmp/agios.conf", 1) != 0) //attention: cannot test it with TWINS 
	{
		printf("TEST\tPANIC! Could not initialize AGIOS!\n");
		exit(1);
	}

	//prepare threads that will "process" requests
	processing_threads=malloc(sizeof(pthread_t)*reqnb);
	processing_threads_index=0;


	//generate requests
	requests = malloc(sizeof(struct req_t)*reqnb);
	files = malloc(sizeof(struct file_t)*filesnb);
	if(files)
	{
		for(i=0; i< filesnb; i++)
		{
			files[i].offset = 0;
			files[i].filename = malloc(sizeof(char)*25);
			if(!files[i].filename)
			{
				alloc_problem = 1;
				break;
			}
			sprintf(files[i].filename, "arquivo.%d.out", i);
		}
	}
	if((!files) || (alloc_problem) || (!requests) || (!processing_threads))
	{
		printf("TEST\tPANIC! Could not allocate memory\n");
		exit(1);
	}
	for(i=0; i<reqnb; i++)
	{
		//choose a file
		int f = rand() % filesnb;
		/*generate a request*/
		requests[i].id = i;
		requests[i].fileid = f;
		requests[i].offset = files[f].offset;
		files[f].offset += req_size + offset_dist; 
		agios_add_request(files[f].filename, REQ_TYPE, requests[i].offset, req_size, (void *) &(requests[i]), &clnt, 0);
		 
		/*wait a while before generating the next one*/
		timeout.tv_sec = time_between / 1000000000L;
		timeout.tv_nsec = time_between % 1000000000L;
		nanosleep(&timeout, NULL);
	}

	/*wait until all requests have been processed*/
	pthread_mutex_lock(&processed_reqnb_mutex);
	while(processed_reqnb < reqnb)
		pthread_cond_wait(&processed_reqnb_cond, &processed_reqnb_mutex);
	pthread_mutex_unlock(&processed_reqnb_mutex);

	//get statistics
	clock_gettime(CLOCK_MONOTONIC, &t1);
	stats = agios_get_statistics_and_reset();
	clock_gettime(CLOCK_MONOTONIC, &t2);
	printf("\n\nStatistics for this execution:\n\n");
	printf("number of requests: %lu, %lu reads and %lu writes\ntime between consecutive requests (ms): avg %.2e (%.2e read and %.2e write) min %lu (%lu read and %lu write) max %lu (%lu read and %lu write)\nrequest size (bytes): avg %.2e (%.2e read and %.2e write) min %lu (%lu read and %lu write) max %lu (%lu read and %lu write)\n", 
	stats->global.total_reqnb, stats->global.total_readnb ,stats->global.total_writenb,
	stats->global.avg_req_time, stats->global.avg_read_req_time, stats->global.avg_write_req_time,
	stats->global.min_req_time, stats->global.min_read_req_time, stats->global.min_write_req_time,
	stats->global.max_req_time, stats->global.max_read_req_time, stats->global.max_write_req_time,
 	stats->global.avg_req_size, stats->global.avg_read_req_size, stats->global.avg_write_req_size,
	stats->global.min_req_size, stats->global.min_read_req_size, stats->global.min_write_req_size,
	stats->global.max_req_size, stats->global.max_read_req_size, stats->global.max_write_req_size); 
	printf("statistics per file:\n");
	for(i=0; i< stats->filenb; i++)
	{
		aux = &(stats->files[i]);

		printf("\tfile %s\n\ttime between requests (ms): avg %.2e (",
			aux->file_id, 
			aux->avg_request_time); 
		if(aux->read_stats) printf("%.2e", aux->read.avg_request_time); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%.2e", aux->write.avg_request_time); else printf("-");
		printf(" write) min %lu (", aux->min_request_time); 
		if(aux->read_stats) printf("%lu", aux->read.min_request_time); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%lu", aux->write.min_request_time); else printf("-");
		printf(" write) max %lu (", aux->max_request_time); 
		if(aux->read_stats) printf("%lu", aux->read.max_request_time); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%lu", aux->write.max_request_time); else printf("-");
		printf(" write)\n\taverage offset distance (bytes): avg %.2e (",
			aux->avg_distance);
		if(aux->read_stats) printf("%.2e", aux->read.avg_distance); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%.2e", aux->write.avg_distance); else printf("-");
		printf(" write)\n\trequest size (bytes): avg "); 
		if(aux->read_stats) printf("%.2e", aux->read.avg_req_size); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%.2e", aux->write.avg_req_size); else printf("-");
		printf(" write min "); 
		if(aux->read_stats) printf("%lu", aux->read.min_req_size); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%lu", aux->write.min_req_size); else printf("-");
		printf(" write max "); 
		if(aux->read_stats) printf("%lu", aux->read.max_req_size); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%lu", aux->write.max_req_size); else printf("-");
		printf(" write\n\ttotal amount of requested data (bytes): "); 
		if(aux->read_stats) printf("%llu", aux->read.received_size); else printf("0");
		printf(" read and ");
		if(aux->write_stats) printf("%llu", aux->write.received_size); else printf("0");
		printf(" write (");
		if(aux->read_stats) printf("%lu", aux->read.receivedreq_nb); else printf("0");
		printf(" requests for read and ");
		if(aux->write_stats) printf("%lu", aux->write.receivedreq_nb); else printf("0");
		printf(" for write)\n\ttotal amount of served data (bytes): ");
		if(aux->read_stats) printf("%llu", aux->read.processed_size); else printf("0");
		printf(" read and ");
		if(aux->write_stats) printf("%llu", aux->write.processed_size); else printf("0");
		printf(" write (");
		if(aux->read_stats) printf("%lu", aux->read.processedreq_nb); else printf("0");
		printf(" requests for read and ");
		if(aux->write_stats) printf("%lu", aux->write.processedreq_nb); else printf("0");
		printf(" for write)\n\tbandwidth (KB/s): "); 
		if(aux->read_stats) printf("%.2e", aux->read.bandwidth); else printf("-");
		printf(" read and ");
		if(aux->write_stats) printf("%.2e", aux->write.bandwidth); else printf("-");
		printf(" write\n\n");
	}
	

	printf("TEST\tIt tool %ld microseconds to call agios_get_statistics_and_reset\n", ((t2.tv_nsec - t1.tv_nsec) + ((t2.tv_sec - t1.tv_sec)*1000000000L))/1000);

	printf("agios_statistics_t has size %lu, the global statistics %lu, each file %lu, each queue %lu\n", sizeof(*stats), sizeof(stats->global), sizeof(stats->files[0]), sizeof(stats->files[0].read));

	//free memory and stop agios
	agios_free_statistics_t(&stats);
	agios_exit();

	for(i=0; i< reqnb; i++)
		pthread_join(processing_threads[i], NULL);
	free(processing_threads);
	for(i=0; i< filesnb; i++)
		free(files[i].filename);
	free(files);
	free(requests);


	return 0;
}
