/* File:	agios.h
 * Created: 	2012 
 * License:	GPL version 3
 * Author:
 *		Francieli Zanon Boito <francielizanon (at) gmail.com>
 *
 * Description:
 *		This file is part of the AGIOS I/O Scheduling tool.
 *		It provides the interface to its users
 *		Further information is available at http://inf.ufrgs.br/~fzboito/agios.html
 *
 * Contributors:
 *		Federal University of Rio Grande do Sul (UFRGS)
 *		INRIA France
 *
 *		inspired in Adrien Lebre's aIOLi framework implementation
 *	
 *		This program is distributed in the hope that it will be useful,
 * 		but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef AGIOS_H
#define AGIOS_H

#ifdef AGIOS_KERNEL_MODULE
#include <linux/completion.h>
#else
#include <pthread.h>
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Request type.
 */
enum {
	RT_READ = 0,
	RT_WRITE = 1,
};

struct client {

	/*
	 * Callback functions
	 */
#ifdef ORANGEFS_AGIOS
	void (*process_request)(int64_t req_id);
	void (*process_requests)(int64_t *reqs, int reqnb);
#elif defined(IOFSL_AGIOS)
	void (*process_request)(unsigned long long int req_id);
	void (*process_requests)(unsigned long long int *reqs, int reqnb);
#else
	void (*process_request)(void * i);
	void (*process_requests)(void ** i, int reqnb);
#endif

	/*
	 * aIOLi calls this to check if device holding file is idle.
	 * Should be supported by FS client
	 */
	int (*is_dev_idle)(void);   //never used
};

struct agios_global_statistics_t 
{ 
	unsigned long int total_reqnb; //Number of requests. We have a similar counter in consumer.c, but this one can be reset, that one is fixed (never set to 0, counts through the whole execution)
	unsigned long int total_readnb;  //number of read requests
	unsigned long int total_writenb;  //number of write requests

	//to measure time between requests
	float avg_req_time;  //average time between requests
	unsigned long int min_req_time; //minimum time between requests
	unsigned long int max_req_time; //maximum time between requests
	float avg_read_req_time; //the same, but only for reads
	unsigned long int min_read_req_time; 
	unsigned long int max_read_req_time; 
	float avg_write_req_time; //the same, but only for writes
	unsigned long int min_write_req_time; 
	unsigned long int max_write_req_time; 
	//to measure request size
	float avg_req_size; //average request size
	unsigned long int min_req_size;  //minimum request size
	unsigned long int max_req_size; //maximum request size
	float avg_read_req_size; //the same, but only for reads
	unsigned long int min_read_req_size;  
	unsigned long int max_read_req_size; 
	float avg_write_req_size; //the same, but only for writes
	unsigned long int min_write_req_size;  
	unsigned long int max_write_req_size; 
};
struct agios_queue_statistics_t 
{
	unsigned long int processedreq_nb; //number of processed requests 
	unsigned long int receivedreq_nb; //number of received requests 

	unsigned long long int processed_size; //total amount of served data
	float bandwidth; //in KB per second

	//statistics on request size
	unsigned long long int received_size; //total amount of received data
	float avg_req_size; //avg request size
	unsigned long int min_req_size;
	unsigned long int max_req_size;

	//statistics on time between requests
	float avg_request_time;
	unsigned long int min_request_time;
	unsigned long int max_request_time;

	//statistics on average offset difference between consecutive requests
	float avg_distance;
};
struct agios_file_statistics_t
{
	char file_id[100]; //if the filename does not fit, it will be truncated

	//statistics on time between requests
	float avg_request_time;
	unsigned long int min_request_time;
	unsigned long int max_request_time;

	//statistics on average offset difference between consecutive requests
	float avg_distance;

	//statistics of the queues
	short int read_stats; //flag to say if the read structure contains valid information or not
	struct agios_queue_statistics_t read;
	short int write_stats; //flag to say if the write structure contains valid information or not
	struct agios_queue_statistics_t write;
};

//times are provided in ms
struct agios_statistics_t
{
	struct agios_global_statistics_t global;
	int filenb;
	struct agios_file_statistics_t *files;
};


#ifdef AGIOS_KERNEL_MODULE
int __init __agios_init(void);
void __exit __agios_exit(void);
#endif
int agios_init(struct client *clnt, char *config_file, int max_app_id); //max_app_id is only relevant for exclusive time window scheduler, otherwise you MUST provide 0. Also we will assume app_id will always be something between 0 and max_app_id. 
void agios_exit(void);


#ifdef ORANGEFS_AGIOS
int agios_add_request(char *file_id, short int type, unsigned long int offset,
		       unsigned long int len, int64_t data, struct client *clnt, unsigned int app_id);
#elif defined(IOFSL_AGIOS)
int agios_add_request(char *file_id, short int type, unsigned long int offset,
		       unsigned long int len, unsigned long long int data, struct client *clnt, unsigned int app_id);
#else
int agios_add_request(char *file_id, short int type, unsigned long int offset,
		       unsigned long int len, void * data, struct client *clnt, unsigned int app_id);
#endif
int agios_release_request(char *file_id, short int type, unsigned long int len, unsigned long int offset, short int is_sub_request, unsigned long long int iofsl_aggregated_req_size);

int agios_set_stripe_size(char *file_id, unsigned int stripe_size);

int agios_cancel_request(char *file_id, short int type, unsigned long int len, unsigned long int offset);

void agios_print_stats_file(char *filename);
void agios_reset_stats(void);

void agios_wait_predict_init(void);

//this function will return a copy of statistics, don't forget to free it after using with the agios_free_statistics_t function! It will also reset said statistics. The important thing to know is that these statistics may be used for automatically selecting the best scheduling policy, and in that case (if you are using a dynamic algorithm such as DYN_TREE or PATTERN_MATCHING), you should never ever call this function.
struct agios_statistics_t *agios_get_statistics_and_reset(void);
void agios_free_statistics_t(struct agios_statistics_t **stats); //function to free the return from agios_get_statistics_and_reset after it was used

#ifdef __cplusplus
}
#endif

#endif // #ifndef AGIOS_H
